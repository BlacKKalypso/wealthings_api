Symfony Api
===========

Paramètres nécessaires
--------------

* PHP 7.4.5 ou plus haut
* PDO-SQLite PHP extension activée

Installation
---------------

[Télécharger Symfony][1] sur votre ordinateur.
Pour avoir mon dépôt git, lancez la commande:
```bash
$ git clone https://gitlab.com/BlacKKalypso/wealthings_api.git
```
    
 Utilisation
---------------

Il n'y a pas besoin de configurer quoique ce soit pour run l'application.
Après s'être mis à la racine du projet wealthings_api et avoir démarré la distribution Apache contenant. 
Ouvrez le terminal et lancez la commande:

```bash
$ php bin/console doctrine:database:create
```

Cette commande sert à créer la base de données "wealthings_api".

Si la commande retourne aucune erreur, Utiliser la commande:

```bash
$ php bin/console make:migration
```
Et:

```bash
$ php bin/console doctrine:migrations:migrate
```

Maintenant vous allez charger les fixtures qui alimenteront les tables de la base de données wealthings_api.
Utilisez la commande:

```bash
$ php bin/console doctrine:fixtures:load
```

C'est bon. Votre api est prête pour être consommée par l'application dans le dépôt[wealthings][2].

[1]: https://symfony.com/download
[2]: https://gitlab.com/BlacKKalypso/wealthings/