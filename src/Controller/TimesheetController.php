<?php


namespace App\Controller;

use Cassandra\Date;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\TimeEntryRepository;
use DateTime;
use Symfony\Component\Serializer\SerializerInterface;


class TimesheetController extends AbstractController
{
    /**
     * @var TimeEntryRepository
     */
    private $timeEntryRepository;

    public function __construct(TimeEntryRepository $timeEntryRepository){
        $this->timeEntryRepository = $timeEntryRepository;
    }

    /**
     * @Route("/my-timesheet/{selectedDate}", name="timesheet", methods={"GET"})
     * @param string $selectedDate
     *
     *
     * @throws \Exception
     */
    public function index(string $selectedDate)
    {
        $date = new DateTime($selectedDate);
        $dayTimesheet = [];

        $totalDay = 0;
        if (!$date){
            throw $this->createNotFoundException(
                'No date found '.$date
            );
        }

        //Todo change detail
        for ($i = 0; $i < 7; $i++) {

            $result = $this->timeEntryRepository->findByDay($date);
            $date = $date->modify('-2 day');


            $totalDay++;

            $dayTimesheet[] = ['date' => $date->format(DateTime::ATOM), 'timeEntries' => $result, 'total' => $totalDay];
        }

        return new JsonResponse($dayTimesheet);
    }
}