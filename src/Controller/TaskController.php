<?php


namespace App\Controller;


use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TaskController extends AbstractController
{
    /**
     * @Route ("my-timesheet/task", name="client_task")
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * */

    public function index(SerializerInterface $serializer){
        //Get base_url/api-v1/my-timesheet/task?clientId=idNum
        //Update endpoint annotation and add parameters
        $task = new Task();
        $serializedTask = $serializer->serialize($task, 'json');

        return JsonResponse::fromJsonString($serializedTask);
    }
}