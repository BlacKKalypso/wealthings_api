<?php


namespace App\Controller;

use App\Entity\TimeEntry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class TimeEntryController extends AbstractController
{
    /**
     * @Route("/my-timesheet", name="new_timesheet", methods={"POST"})
     * @param SerializerInterface $serializer
     * */
    public function create(SerializerInterface $serializer)
    {

        $request = Request::createFromGlobals();
        $data = json_decode($request->getContent());

        $newTimeEntry = new TimeEntry();
        $newTimeEntry
            ->setDescription($data->description)
        ->setCommandId($data->commandId)
        ->setCommandName('toto')
        ->setTaskId($data->taskId)
        ->setTaskName('toto')
        ->setDate('2020-08-31T00:00:00.000Z')
        ->setStartTime('2020-08-31T00:00:00.000Z')
        ->setEndTime(null)
        ->setTime(null)
        ->setIsBillable(true)
        ->setIsLocked(false)
        ->setClientId($data->clientId)
        ->setClientName('toto');

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($newTimeEntry);
        $entityManager->flush();

        $serializedNewTimeEntry = $serializer->serialize($newTimeEntry, 'json');

        return JsonResponse::fromJsonString($serializedNewTimeEntry);
    }
}