<?php


namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="login_check")
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * */

    public function index(SerializerInterface $serializer){
        //Return logged user information
        $user = $this->getUser();
        $serializedUser = $serializer->serialize($user, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['password']]);

        return JsonResponse::fromJsonString($serializedUser);
    }
}