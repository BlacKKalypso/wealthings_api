<?php


namespace App\Controller;


use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class ClientController extends AbstractController
{
    //Get base_url/api-v1/my-timesheet/client
    //Todo endpoint show all the client in alphabetic order

    /**
     * @Route("my-timesheet/client", name="client")
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * */
    public function index(SerializerInterface $serializer)
    {
        $client = new Client();
        $serializedClient = $serializer->serialize($client, 'json');

        return JsonResponse::fromJsonString($serializedClient);

    }
}