<?php


namespace App\Controller;

use App\Entity\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class CommandController extends AbstractController
{

    /**
     * @Route ("/my-timesheet/order", name="client_order")
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * */
    public function index(SerializerInterface $serializer)
    {
        $command = new Command();
        $serializedCommand = $serializer->serialize($command, 'json');

        return JsonResponse::fromJsonString($serializedCommand);
    }
}