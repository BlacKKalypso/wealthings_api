<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/homepage", name="homepage")
     */
    public function greedings(){
        $user = $this->getUser();
        $pageTitle = "Homepage";

        return $this->render('homepage.html.twig', [
            'user' => $user,
            'page_title' => $pageTitle
        ]);
    }
}