<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class UserFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();
        $user->setLastName('Tonye');
        $user->setFirstName('Nancy');
        $user->setMiddleName(null);
        $user->setUsername($user->getFirstName() . "." . $user->getLastname());
        $user->setGender('FEMALE');
        $user->setIsEmployed(false);
        $user->setIsActive(true);
        $user->setBirthday('1994-03-09T00:00:00.000Z');
        $user->setAvatarId(null);
        $user->setDefaultLanguage('FR');
        $user->setOrganisationId(1);
        $user->setEmail('tonyenancy@gmail.com');
        $user->setRoles((array)'ROLE_USER');
        $user->setToken('e500a48c-3ded-4d50-9857-62020ee0af92');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'password'
        ));

        $manager->persist($user);

        $manager->flush();
    }
}
