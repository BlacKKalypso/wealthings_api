<?php

namespace App\DataFixtures;

use App\Entity\Command;
use App\Repository\ClientRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;


class CommandFixtures extends Fixture implements DependentFixtureInterface
{
    private $clients;
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository){
        $this->clientRepository = $clientRepository;
    }
    public function load(ObjectManager $manager)
    {
        $this->clients = $this->clientRepository->findAll();

        $commandsName = array(
            "Maintaining the application",
            "Organize a party",
            "Project development",
            "Having appointment with the client",
            "Prepare a new session to teach usage of the product",
            "Organize the christmas party"
        );

        foreach($commandsName as $commandName){
            $command = $this->createCommand($commandName);
            $manager->persist($command);

        }

        $manager->flush();
    }

    private function createCommand($name){
        $commandCreated = new Command();

        shuffle($this->clients);
        $client  = $this->clients[0];

        $commandCreated->setName($name);
        $commandCreated->setClient($client);

        return $commandCreated;
    }
    public function getDependencies()
    {
        return array(
            ClientFixtures::class
        );
    }
}
