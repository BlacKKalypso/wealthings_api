<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Repository\CommandRepository;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{

    private $commands;
    private $commandRepository;

    public function __construct(CommandRepository $commandRepository)
    {
        $this->commandRepository = $commandRepository;
    }

        /**
         * @inheritDoc
         */
        public function load(ObjectManager $manager)
    {
        $this->commands = $this->commandRepository->findAll();

        $tasksName = array("Clean the car", "Maintain the website", "Take a break"
        );
        foreach ($tasksName as $taskName) {
            $task = $this->createTask($taskName);
            $manager->persist($task);

            $manager->persist($task);
        }

        $manager->flush();
    }

        private function createTask($name){
        $taskCreate = new Task();

        $command = $this->commands[0];
        shuffle($this->commands);

        $taskCreate->setName($name);
        $taskCreate->setCommand($command);

        return $taskCreate;
    }

    public function getDependencies()
    {
        return array(
            CommandFixtures::class
        );
    }
}
