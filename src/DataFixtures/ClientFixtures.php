<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $array = array("Diana Prince, Wonder Woman",
            "Arthur Fleck, Joker",
            "Arthur Curry, Aquaman",
            "Harleen Frances Quinzel, Harley Quinn",
            "Peter Parker, Spider-man",
            "Tony Stark, Iron man",
            "Natasha Romanoff, Black widow",
            "Clint Barton, Hawkeye",
            "Steve Rogers, Captain America"
        );

        foreach ($array as $clientName){
            $client = new Client();
            $client->setName($clientName);
            $manager->persist($client);
        }

        $manager->flush();
    }
}
