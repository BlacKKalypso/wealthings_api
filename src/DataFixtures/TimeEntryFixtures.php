<?php


namespace App\DataFixtures;

use App\Repository\ClientRepository;
use App\Repository\CommandRepository;
use App\Repository\TaskRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
Use App\DataFixtures\CommandFixtures;
use Doctrine\Persistence\ObjectManager;
use App\Entity\TimeEntry;


class TimeEntryFixtures extends Fixture implements DependentFixtureInterface
{
    private $clients;
    private $clientRepository;
    private $commands;
    private $commandRepository;
    private $tasks;
    private $taskRepository;

    public function __construct(ClientRepository $clientRepository, CommandRepository $commandRepository, TaskRepository $taskRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->commandRepository = $commandRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        //Create loop of name and random like
        $this->clients = $this->clientRepository->findAll();
        $this->commands = $this->commandRepository->findAll();
        $this->tasks = $this->taskRepository->findAll();

        for ($i = 0; $i < 20; $i++) {
            $timeEntry = $this->createTimeEntry();
            $manager->persist($timeEntry);
        }

        $manager->flush();
    }

    /**
     * @throws \Exception
     * */
    private function createTimeEntry()
    {
        $entry = new TimeEntry();


        $description = array("",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "Tortor vitae purus faucibus ornare suspendisse sed.",
            "Amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus.",
            "Leo a diam sollicitudin tempor id eu nisl.",
            "Ac turpis egestas integer eget aliquet nibh praesent.",
            "Ultrices vitae auctor eu augue ut.");
        shuffle($description);

        $dates = array("2020-08-24T00:00:00.00Z",
            "2020-08-25T00:00:00.00Z",
            "2020-08-26T00:00:00.00Z",
            "2020-08-27T00:00:00.00Z",
            "2020-08-28T00:00:00.00Z",
            "2020-08-28T00:00:00.00Z",
            "2020-08-29T00:00:00.00Z",
            "2020-08-30T00:00:00.00Z",
            "2020-08-31T00:00:00.00Z",
            "2020-08-01T00:00:00.00Z",
            "2020-09-02T00:00:00.000Z"
        );
        shuffle($dates);

        $dateToTime = strtotime($dates[0]);
        $endTime = $dateToTime + rand(0, 1440);
        $endDate = \date(DATE_ATOM, $endTime);


        $startTime = $dateToTime;
        $timeDiff = abs($endTime - $startTime);
        $timeDiffHour = round($timeDiff / (60*60), 2);


        $client = $this->clients[0];
        shuffle($this->clients);

        $command = $this->commands[0];
        shuffle($this->commands);

        $task = $this->tasks[0];
        shuffle($this->tasks);

        $entry
            ->setDescription($description[0])
            ->setStatus("UNSUBMITTED")
            ->setIsBillable(true)
            ->setDate($dates[0])
            ->setStartTime($dates[0])
            ->setEndTime($endDate)
            ->setClientId($client->getid())
            ->setClientName($client->getName())
            ->setCommandId($command->getId())
            ->setCommandName($command->getName())
            ->setTask($task)
            ->setTaskName($task->getName())
            ->setTime(strval($timeDiffHour))
            ->setIsLocked(false);
        //dump($task);
        //die();
        return $entry;

    }

    public function getDependencies()
    {
        // TODO: Implement getDependencies() method.
        return array(
            ClientFixtures::class,
            CommandFixtures::class,
            TaskFixtures::class
        );
    }
}