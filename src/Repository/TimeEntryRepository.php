<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Command;
use App\Entity\Task;
use App\Entity\TimeEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method TimeEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeEntry[]    findAll()
 * @method TimeEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeEntry::class);
    }


    public function findByDay(DateTime $day): ?array
    {
        $dateStart = new DateTime($day->format('r'));
        $dateEnd = new DateTime($day->format('r'));
        $dateEnd->modify('+1 day');

        //calculate 24h later from the given date
        return $this->createQueryBuilder('t')
            //->from('App:TimeEntry t')
            ->andWhere('t.startTime >= :dayStart')
            ->andWhere('t.endTime < :dayEnd')
            ->setParameter('dayStart', $dateStart)
            ->setParameter('dayEnd', $dateEnd)
            ->getQuery()
            ->getArrayResult();
    }
}
