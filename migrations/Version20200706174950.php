<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706174950 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE time_entry (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, isbillable TINYINT(1) NOT NULL, date VARCHAR(255) NOT NULL, start_time VARCHAR(255) DEFAULT NULL, end_time VARCHAR(255) DEFAULT NULL, time VARCHAR(255) DEFAULT NULL, client_id INT NOT NULL, client_name VARCHAR(255) NOT NULL, order_id INT NOT NULL, order_name VARCHAR(255) NOT NULL, task_id INT DEFAULT NULL, task_name VARCHAR(255) DEFAULT NULL, is_locked TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE time_entry');
    }
}
