<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820160754 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_entry ADD client_id INT NOT NULL, ADD client_name VARCHAR(255) NOT NULL, ADD order_id INT NOT NULL, ADD order_name VARCHAR(255) NOT NULL, ADD task_id INT DEFAULT NULL, ADD task_name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_entry DROP client_id, DROP client_name, DROP order_id, DROP order_name, DROP task_id, DROP task_name');
    }
}
