<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200728121458 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD first_name VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) NOT NULL, ADD organisation_id INT NOT NULL, ADD default_language VARCHAR(100) NOT NULL, ADD middle_name VARCHAR(255) DEFAULT NULL, ADD is_employed TINYINT(1) NOT NULL, ADD is_active TINYINT(1) NOT NULL, ADD gender VARCHAR(255) NOT NULL, ADD birthday VARCHAR(255) NOT NULL, ADD avatar_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP first_name, DROP lastname, DROP organisation_id, DROP default_language, DROP middle_name, DROP is_employed, DROP is_active, DROP gender, DROP birthday, DROP avatar_id');
    }
}
