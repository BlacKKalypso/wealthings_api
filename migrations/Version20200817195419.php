<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200817195419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_entry DROP FOREIGN KEY FK_6E537C0CABDD46BE');
        $this->addSql('DROP INDEX IDX_6E537C0CABDD46BE ON time_entry');
        $this->addSql('ALTER TABLE time_entry DROP timesheet_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_entry ADD timesheet_id INT NOT NULL');
        $this->addSql('ALTER TABLE time_entry ADD CONSTRAINT FK_6E537C0CABDD46BE FOREIGN KEY (timesheet_id) REFERENCES timesheet (id)');
        $this->addSql('CREATE INDEX IDX_6E537C0CABDD46BE ON time_entry (timesheet_id)');
    }
}
